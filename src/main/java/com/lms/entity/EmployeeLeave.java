package com.lms.entity;


public class EmployeeLeave {

	private String Id;
	private String NoOfLeavesAvailed;
	private String NoOfLeavesRemaining;
	private String TotalLeavesAvailable;
	private boolean isApplied;
	private boolean isApproved;
	private String FromDate;
	private String ToDate;
	 private Leave type;

	


	public EmployeeLeave() {

	}

	public EmployeeLeave(String Id, String NoOfLeavesAvailed, String NoOfLeavesRemaining,String TotalLeavesAvailable, boolean isApplied,
			boolean isApproved, String FromDate, String ToDate,Leave type) {
		this.Id = Id;
		this.NoOfLeavesRemaining = NoOfLeavesRemaining;
		this.NoOfLeavesAvailed = NoOfLeavesAvailed;
		this.TotalLeavesAvailable=TotalLeavesAvailable;
		this.isApplied = isApplied;
		this.isApproved = isApproved;
		this.FromDate = FromDate;
		this.ToDate = ToDate;
		this.type=type;

	}

	public String getTotalLeavesAvailable() {
		return TotalLeavesAvailable;
	}

	public void setTotalLeavesAvailable(String totalLeavesAvailable) {
		TotalLeavesAvailable = totalLeavesAvailable;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getNoOfLeavesAvailed() {
		return NoOfLeavesAvailed;
	}

	public void setNoOfLeavesAvailed(String noOfLeavesAvailed) {
		NoOfLeavesAvailed = noOfLeavesAvailed;
	}

	public String getNoOfLeavesRemaining() {
		return NoOfLeavesRemaining;
	}

	public void setNoOfLeavesRemaining(String noOfLeavesRemaining) {
		NoOfLeavesRemaining = noOfLeavesRemaining;
	}

	public boolean isApplied() {
		return isApplied;
	}

	public void setApplied(boolean isApplied) {
		this.isApplied = isApplied;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}

	public String getFromDate() {
		return FromDate;
	}

	public void setFromDate(String fromDate) {
		FromDate = fromDate;
	}

	public String getToDate() {
		return ToDate;
	}

	public void setToDate(String toDate) {
		ToDate = toDate;
	}
	public Leave getType() {
		return type;
	}

	public void setType(Leave type) {
		this.type = type;
	}


}
