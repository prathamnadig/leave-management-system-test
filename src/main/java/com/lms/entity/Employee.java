package com.lms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

@Table(name="Employee")
public class Employee {
	@Id
	private int id;
	@Column
	private String name;
	@Column
	private String designation;
	
	@Column
	private String noOfLeaves;
	@Column
	private Boolean isProbation;
	@Column
	private String totalLeavesAvailable;
	@Column
	private String noOfLeavesAvailed;
	@Column
	private Boolean isApprovedLeave;

	public Boolean getIsApprovedLeave() {
		return isApprovedLeave;
	}

	public void setIsApprovedLeave(Boolean isApprovedLeave) {
		this.isApprovedLeave = isApprovedLeave;
	}

	public String getNoOfLeaves() {
		return noOfLeaves;
	}

	public void setNoOfLeaves(String noOfLeaves) {
		this.noOfLeaves = noOfLeaves;
	}

	public Employee() {

	}

	public Employee(int id, String name, String designation, Boolean isProbation, String totalLeavesAvailable,
			String noOfLeavesAvailed, Boolean isApprovedLeave) {
		this.id = id;
		this.name = name;
		this.designation = designation;
		this.isProbation = isProbation;
		this.totalLeavesAvailable = totalLeavesAvailable;
		this.noOfLeavesAvailed = noOfLeavesAvailed;
		this.isApprovedLeave = isApprovedLeave;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Boolean getIsProbation() {
		return isProbation;
	}

	public void setIsProbation(Boolean isProbation) {
		this.isProbation = isProbation;
	}

	public String getTotalLeavesAvailable() {
		return totalLeavesAvailable;
	}

	public void setTotalLeavesAvailable(String totalLeavesAvailable) {
		this.totalLeavesAvailable = totalLeavesAvailable;
	}

	public String getNoOfLeavesAvailed() {
		return noOfLeavesAvailed;
	}

	public void setNoOfLeavesAvailed(String noOfLeavesAvailed) {
		this.noOfLeavesAvailed = noOfLeavesAvailed;
	}

}
