package com.lms.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lms.entity.Employee;
import com.lms.service.EmployeeService;
import com.lms.service.EmployeeServiceImpl;

import net.bytebuddy.asm.Advice.Return;

@RestController
@RequestMapping(value = "/employee")

public class EmpController {

	EmployeeService empService = new EmployeeServiceImpl();

	@RequestMapping(value = "/getAllEmployees", method = RequestMethod.GET, produces = "application/json")
	public List<Employee> getAllEmployee() {
		List<Employee> empList = null;
		try {
			empList = empService.getAllEmployee();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return empList;
	}

	@RequestMapping(value = "/getEmployeeDetailsById", method = RequestMethod.GET, produces = "application/json")
	public Employee getEmployeeDetailsById(@RequestParam int id) {
		Employee emp = null;
		try {
			emp = empService.getEmployeeDetailsById(id);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return emp;
	}

	@RequestMapping(value = "/getEmployeeInProbation", method = RequestMethod.GET, produces = "application/json")
	public Employee getEmployeeInProbation(int id) {
		Employee emp = null;
		try {

			emp = empService.getEmployeeInProbation(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return emp;

	}

	@RequestMapping(value = "/getEmployeeLeavesByName", method = RequestMethod.GET, produces = "application/json")
	public Employee getEmployeeLeavesByName(String name) {
		Employee emp = null;
		try {
			emp = empService.getEmployeeLeavesByName(name);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return emp;
	}

	@RequestMapping(value = "/saveEmployeeDetails", method = RequestMethod.GET, produces = "application/json")
	public Boolean saveEmployeeDetails(@RequestParam int id, @RequestParam String name,
			@RequestParam String designation, @RequestParam Boolean isProbation,
			@RequestParam String totalLeavesAvailable, @RequestParam String noOfLeavesAvailed,
			@RequestParam Boolean isApprovedLeave) {
		Employee emp = null;
		try {
			boolean flag = empService.saveEmployeeDetails(id, name, designation, isProbation, totalLeavesAvailable,
					noOfLeavesAvailed, isApprovedLeave);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	@RequestMapping(value = "/updateEmployeeDetails", method = RequestMethod.GET, produces = "application/json")
	public Employee updateEmployeeDetailsById(@RequestParam int id, @RequestParam String name,
			@RequestParam String designation, @RequestParam Boolean isProbation,
			@RequestParam String totalLeavesAvailable, @RequestParam String noOfLeavesAvailed,
			@RequestParam Boolean isApprovedLeave) {

		Employee emp = null;
		try {

			emp = empService.updateEmployeeDetailsById(id, name, designation, isProbation, totalLeavesAvailable,
					noOfLeavesAvailed, isApprovedLeave);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return emp;
	}

}