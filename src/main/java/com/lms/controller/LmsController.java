package com.lms.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.lms.dao.EmployeeDao;
import com.lms.entity.Employee;
import com.lms.entity.EmployeeLeave;
import com.lms.service.LmsServiceImpl;

@RestController
@RequestMapping(value = "/emp")
public class LmsController {

	LmsServiceImpl lmsService = new LmsServiceImpl();

	@RequestMapping(value = "/unused_leaves", method = RequestMethod.GET, produces = "application/json")

	public String getRemainingLeavesById(@RequestParam int id) {
		// Employee e = new Employee();
		// e.setNoOfLeaves("2");

		// lmsService.getRemainingNoOfLeavesById(id);

		return lmsService.getRemainingNoOfLeavesById(id);
	}

	@RequestMapping(value = "/leaves", method = RequestMethod.GET, produces = "application/json")

	public int getRemainingLeavesByName(@RequestParam String name) {
		Employee e = new Employee();
		// e.setNoOfLeaves(3);
		return 2;

	}

	@RequestMapping(value = "/approved", method = RequestMethod.GET, produces = "application/json")
	public Boolean getApprovalByManager(@RequestParam int apply, String leaveType) {
		return true;
	}

	@RequestMapping(value = "/total_leaves", method = RequestMethod.GET, produces = "application/json")

	public String getTotalNoOfLeaves(@RequestParam int id) {

		return lmsService.getTotalNoOfLeaves(id);
	}

	@RequestMapping(value = "/employeeLeaves", method = RequestMethod.GET, produces = "application/json")
	public void displayAllEmployeeRemainingLeaves() {
		// TODO Auto-generated method stub

	}

	@RequestMapping(value = "/leaves_probation", method = RequestMethod.GET, produces = "application/json")
	public Employee getTotalNoOfLeavesInProbationById(@RequestParam int id) {
		// TODO Auto-generated method stub
		return lmsService.getTotalNoOfLeavesInProbationById(id);
	}

	@RequestMapping(value = "/leave/status", method = RequestMethod.GET, produces = "application/json")
	public String getStatusOfLeavesAppliedById(int id) {
		// TODO Auto-generated method stub
		return lmsService.getStatusOfLeavesAppliedById(id);
	}

	@RequestMapping(value = "/apply", method = RequestMethod.GET, produces = "application/json")
	public EmployeeLeave applyForLeave(@RequestParam int Id,@RequestParam String type,@RequestParam String FromDate,@RequestParam String ToDate) {
		// TODO Auto-generated method stub
		return lmsService.applyForLeave(Id, type, FromDate, ToDate);
	}

	@RequestMapping(value = "/all_employee_leave", method = RequestMethod.GET, produces = "application/json")
	public List<EmployeeLeave> getEmployeeLeave() {

		return lmsService.getEmployeeLeave();
	}

	@RequestMapping(value = "/all_employee", method = RequestMethod.GET, produces = "application/json")
	public List<Employee> getEmployee() {
		return lmsService.getEmployee();
	}
}
