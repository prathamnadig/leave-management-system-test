package com.lms.service;

import java.util.List;

import com.lms.entity.Employee;
import com.lms.entity.EmployeeLeave;

public interface LmsService {
	
	public List<Employee> getEmployee();
	
	public List<EmployeeLeave> getEmployeeLeave();
	
	public String getRemainingNoOfLeavesById(int id);

	public String getRemainingNoOfLeavesByName(String name);

	public String getTotalNoOfLeaves(int id);

	// public int getNoOfLeavesByDesignation(String designation);
	public String displayAllEmployeeRemainingLeaves();

	// public void displayAllEmployeeRemainingLeavesByTeam(int teamId);
	public Employee getTotalNoOfLeavesInProbationById(int id);

	public String getStatusOfLeavesAppliedById(int id);

	public EmployeeLeave applyForLeave(int Id, String type,String FromDate, String ToDate);
	
	//public void cancelLeaveApplied()
}
