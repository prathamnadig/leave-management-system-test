package com.lms.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.lms.dao.EmployeeDaoImpl;
import com.lms.entity.Employee;
import com.lms.entity.EmployeeLeave;

@Service
public class LmsServiceImpl implements LmsService {

	EmployeeDaoImpl empDaoImpl = new EmployeeDaoImpl();
	Employee e = new Employee();

	public String getRemainingNoOfLeavesById(int id) {

		EmployeeLeave employeeLeave = empDaoImpl.getRemainingNoOfLeavesById(id);
		String leavesRemaining = employeeLeave.getNoOfLeavesRemaining();
		return leavesRemaining;
	}

	public String getRemainingNoOfLeavesByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getTotalNoOfLeaves(int id) {
		EmployeeLeave employeeLeave = empDaoImpl.getTotalNoOfLeaves(id);
		String totalLeaves = employeeLeave.getTotalLeavesAvailable();
		return totalLeaves;
	}

	// public List<EmployeeLeave> displayAllEmployeeRemainingLeaves() {
//
	// EmployeeLeave employeeLeave = empDaoImpl.displayAllEmployeeRemainingLeaves();
	// List<EmployeeLeave> disp=employeeLeave.getTotalLeavesAvailable();

	// }

	public Employee getTotalNoOfLeavesInProbationById(int Id) {
		// Employee e = empDaoImpl.getTotalNoOfLeavesInProbationById();
		Employee leavesInProb = empDaoImpl.getTotalNoOfLeavesInProbationById(Id);

//Integer s=Integer.parseInt(empDaoImpl.getTotalNoOfLeavesInProbationById(Id));
		return leavesInProb;
	}

	public String getStatusOfLeavesAppliedById(int id) {
		String e = empDaoImpl.getStatusOfLeavesAppliedById(id);
		// String status = empDaoImpl.getStatusOfLeavesAppliedById(id);

		return e;
	}

	public EmployeeLeave applyForLeave(int Id, String type, String FromDate, String ToDate) {
		// TODO Auto-generated method stub
//EmployeeLeave e=empDaoImpl.applyForLeave(Id, type, FromDate, ToDate);
//String s=e.
		EmployeeLeave e = empDaoImpl.applyForLeave(Id, type, FromDate, ToDate);

		return e;

	}

	public String displayAllEmployeeRemainingLeaves() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<EmployeeLeave> getEmployeeLeave() {
		List<EmployeeLeave> empLeave = empDaoImpl.getEmployeeLeave();

		// EmployeeLeave empLeave = (EmployeeLeave) empDaoImpl.getEmployee();
		return empLeave;

		// List<EmployeeLeave> empLeaveList=empLeave.get
	}

	public List<Employee> getEmployee() {
		List<Employee> emp=empDaoImpl.getEmployee();
		
		
		return emp;
	}

}
