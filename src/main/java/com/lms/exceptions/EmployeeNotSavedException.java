package com.lms.exceptions;

public class EmployeeNotSavedException extends Exception{
	String message;

	public EmployeeNotSavedException(String message) {
		this.message = message;
	}
}
