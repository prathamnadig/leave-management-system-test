package com.lms.dao;

import java.util.ArrayList;


import java.util.List;

import javax.transaction.Transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.lms.entity.Employee;
import com.lms.entity.EmployeeLeave;
import com.lms.entity.Leave;
import com.lms.hibernateC.HibernateConfiguration;

public class EmployeeDaoImpl implements EmployeeDao {
	
	public List<Employee> getEmployee() {

		Employee e1 = new Employee(1, "Pratham", "Trainee Software Engineer", true, "30", "10", false);
		Employee e2 = new Employee(2, "Praveen", "Trainee Software Engineer", true, "30", "5", false);
		Employee e3 = new Employee(3, "Suhas", "Software Engineer 1", false, "30", "9", false);
		Employee e4 = new Employee(4, "Harishwar", "Software Engineer 1", true, "30", "4", false);
		Employee e5 = new Employee(5, "Vishnu", "Software Engineer 1", false, "30", "3", false);
		Employee e6 = new Employee(6, "Sreedip", "Software Engineer 2", false, "30", "8", false);
		Employee e7 = new Employee(7, "Rathan", "Trainee Software Engineer", true, "30", "4", false);
		Employee e8 = new Employee(8, "Shubam", "Software Engineer 2", false, "30", "16", false);
		Employee e9 = new Employee(9, "Thejasvi", "Software Engineer 1", true, "30", "10", false);
		Employee e10 = new Employee(10, "Pavan", "Software Engineer 3", true, "30", "3", false);

		List<Employee> empList = new ArrayList<Employee>();
		empList.add(e1);
		empList.add(e2);
		empList.add(e3);
		empList.add(e4);
		empList.add(e5);
		empList.add(e6);
		empList.add(e7);
		empList.add(e8);
		empList.add(e9);
		empList.add(e10);

		return empList;

	}
	@Autowired
	private SessionFactory sessionFactory;
	

	
	
	/**
	 * method - addEmp
	 * create obj HC . obj.getSF();
	 * sf.openSession -> Session
	 * session.save(emp)
	 * @return
	 */

	public List<EmployeeLeave> getEmployeeLeave() {
		EmployeeLeave el1 = new EmployeeLeave("1", "2", "28", "30", false, false, null, null, null);
		EmployeeLeave el2 = new EmployeeLeave("2", "3", "27", "30", false, false, null, null, null);
		EmployeeLeave el3 = new EmployeeLeave("3", "4", "26", "30", false, false, null, null, null);
		EmployeeLeave el4 = new EmployeeLeave("4", "0", "30", "30", false, false, null, null, null);
		EmployeeLeave el6 = new EmployeeLeave("5", "1", "29", "30", false, false, null, null, null);
		EmployeeLeave el5 = new EmployeeLeave("6", "1", "29", "30", false, false, null, null, null);

		List<EmployeeLeave> empLeaveList = new ArrayList<EmployeeLeave>();
		empLeaveList.add(el1);
		empLeaveList.add(el2);
		empLeaveList.add(el3);
		empLeaveList.add(el4);
		empLeaveList.add(el5);
		empLeaveList.add(el6);

		return empLeaveList;

	}
	
	
	@Override
	public void addEmployee(Employee e) {
	
		Session session=this.sessionFactory.getCurrentSession();
		session.persist(e);
		System.out.println("Employee saved successfully");
		
		
	}
	
	@Override
	public EmployeeLeave getRemainingNoOfLeavesById(int id) {

	Session session=this.sessionFactory.getCurrentSession();
	//Transaction e=session.beginTransaction();
		Employee e=(Employee) session.get(Employee.class, id);
		List<EmployeeLeave> empLeaveList = getEmployeeLeave();
		EmployeeLeave remainingLeavesByIdObj = empLeaveList.get(id);
		remainingLeavesByIdObj.getNoOfLeavesRemaining();

		return remainingLeavesByIdObj;
	}

	/*
	 * NOT IMPLEMENTING THIS METHOD AS THERE IS NO EMPLOYEE_NAME COLUMN IN THE
	 * EmployeeLeave TABLE
	 */
	public String getRemainingNoOfLeavesByName(String name) {
		return null;
	}

	public EmployeeLeave getTotalNoOfLeaves(int id) {
		List<EmployeeLeave> empLeaveList = getEmployeeLeave();
		EmployeeLeave totalNoOfLeavesObj = empLeaveList.get(id);
		totalNoOfLeavesObj.getTotalLeavesAvailable();
		return totalNoOfLeavesObj;
	}

	public List<EmployeeLeave> displayAllEmployeeRemainingLeaves() {
		List<EmployeeLeave> empLeaveList = getEmployeeLeave();
		List<EmployeeLeave> empRemainingLeaveList = new ArrayList<EmployeeLeave>();
		for (EmployeeLeave s : empLeaveList) {
			if (empLeaveList.isEmpty() == false) {
				// prints the whole list instead of printing the remaining leaves only
				empRemainingLeaveList.add(s);

			}
		}

		return empRemainingLeaveList;
	}

	public Employee getTotalNoOfLeavesInProbationById(int id) {
		List<Employee> empList = getEmployee();

		List<Employee> leavesInProb = new ArrayList<Employee>();
		Employee e1 = new Employee();

		for (Employee e : empList) {
			if (e.getIsProbation() == true) {
				if (e.getId()==id)

				{
					e1.setDesignation(e.getDesignation());
					e1.setId(e.getId());
					e1.setIsApprovedLeave(e.getIsApprovedLeave());
					e1.setIsProbation(e.getIsProbation());
					e1.setName(e.getName());
					e1.setNoOfLeaves(e.getNoOfLeaves());
					e1.setNoOfLeavesAvailed(e.getNoOfLeavesAvailed());
					e1.setTotalLeavesAvailable(e.getTotalLeavesAvailable());
				}

			}

		}
		// System.out.println(leavesInProb);
		return e1;

	}

	public String getStatusOfLeavesAppliedById(int id) {
		List<EmployeeLeave> empLeaveList = getEmployeeLeave();
		EmployeeLeave employeeLeave = empLeaveList.get(id);
		String status = null;
		if (employeeLeave.isApplied() == true) {
			status = "Applied";
		} else if (employeeLeave.isApplied() == false) {
			status = "Not applied";
		} else if (employeeLeave.isApproved() == true) {
			status = "Approved";
		}

		return status;
	}

	public EmployeeLeave applyForLeave(int Id, String type, String FromDate, String ToDate) {
		List<EmployeeLeave> empLeaveList = getEmployeeLeave();
		EmployeeLeave el1 = empLeaveList.get(Id);

		if (Leave.SICK_LEAVE.toString().equals(type)) {
			el1.setType(Leave.SICK_LEAVE);
			el1.setApplied(true);
			el1.setApproved(false);

			Integer noOfLeaves = Integer.parseInt(ToDate) - Integer.parseInt(FromDate);
			el1.setNoOfLeavesAvailed(Integer.toString(noOfLeaves));
			String totalLeaves = el1.getTotalLeavesAvailable();

			Integer noOfRemainingLeaves = Integer.parseInt(totalLeaves) - noOfLeaves;
			el1.setNoOfLeavesRemaining(Integer.toString(noOfRemainingLeaves));
		}

		if (Leave.CASUAL_LEAVE.toString().equals(type)) {
			el1.setType(Leave.CASUAL_LEAVE);
			el1.setApplied(true);

			el1.setApproved(false);
			Integer noOfLeaves = Integer.parseInt(ToDate) - Integer.parseInt(FromDate);
			el1.setNoOfLeavesAvailed(Integer.toString(noOfLeaves));
			String totalLeaves = el1.getTotalLeavesAvailable();

			Integer noOfRemainingLeaves = Integer.parseInt(totalLeaves) - noOfLeaves;
			el1.setNoOfLeavesRemaining(Integer.toString(noOfRemainingLeaves));

		}

		if (Leave.EARNED_LEAVE.toString().equals(type)) {
			el1.setType(Leave.CASUAL_LEAVE);
			el1.setApplied(true);

			el1.setApproved(false);
			Integer noOfLeaves = Integer.parseInt(ToDate) - Integer.parseInt(FromDate);
			el1.setNoOfLeavesAvailed(Integer.toString(noOfLeaves));
			String totalLeaves = el1.getTotalLeavesAvailable();

			Integer noOfRemainingLeaves = Integer.parseInt(totalLeaves) - noOfLeaves;
			el1.setNoOfLeavesRemaining(Integer.toString(noOfRemainingLeaves));

		}

		if (Leave.OPTIONAL_LEAVE.toString().contentEquals(type)) {
			el1.setType(Leave.EARNED_LEAVE);
			el1.setApplied(true);
			el1.setApproved(false);

			Integer noOfLeaves = Integer.parseInt(ToDate) - Integer.parseInt(FromDate);
			el1.setNoOfLeavesAvailed(Integer.toString(noOfLeaves));

			String totalLeaves = el1.getTotalLeavesAvailable();
			Integer noOfLeaveRemaining = Integer.parseInt(totalLeaves) - noOfLeaves;

			el1.setNoOfLeavesRemaining(Integer.toString(noOfLeaveRemaining));

		}
		return el1;
	}

}
