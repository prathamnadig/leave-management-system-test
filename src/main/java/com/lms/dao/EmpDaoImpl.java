package com.lms.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.lms.entity.Employee;
import com.lms.exceptions.EmployeeNotFoundException;
import com.lms.exceptions.EmployeeNotSavedException;

public class EmpDaoImpl implements EmpDao {

	private SessionFactory getSessionFactory() {
		Configuration config = new Configuration().configure("hibernate.cfg.xml");
		SessionFactory sessionFactory = config.buildSessionFactory();
		return sessionFactory;

	}

	public List<Employee> getAllEmployee() throws EmployeeNotFoundException {
		Session session = getSessionFactory().openSession();
		Criteria qry = session.createCriteria("* from Employee");
		List<Employee> empList = new ArrayList<Employee>();

		if (!qry.list().isEmpty()) {
			List<Employee> emp = qry.list();
			session.clear();
		}
		return empList;
	}

	@Override
	public Employee getEmployeeDetailsById(int id) throws EmployeeNotFoundException {

		Session session = getSessionFactory().openSession();

		// Criteria qry = session.createCriteria(Employee.class);

		Employee emp = (Employee) session.get(Employee.class, id);

		if (emp == null) {
			throw new EmployeeNotFoundException("Employee not found");

		}
		session.save(emp);
		return emp;
	}

	@Override
	public Employee getEmployeeInProbation(int id) throws EmployeeNotFoundException {
		Configuration config = new Configuration().configure("hibernate.cfg");
		SessionFactory sessionFactory = config.buildSessionFactory();

		Session session = getSessionFactory().openSession();

		Employee empInProb = (Employee) session.get(Employee.class, id);
		if (empInProb == null) {
			throw new EmployeeNotFoundException("Employee not found");
		}
		session.save(empInProb);

		return empInProb;

	}

	@Override
	public Employee getEmployeeLeavesByName(String name) throws EmployeeNotFoundException {

		Session session = getSessionFactory().openSession();

		Employee empLeavesByName = (Employee) session.get(Employee.class, name);
		if (empLeavesByName == null) {
			throw new EmployeeNotFoundException("employee not found ");
		}
		session.save(empLeavesByName);

		return empLeavesByName;

	}

	@Override
	public Employee getEmployeePosition(String name) throws EmployeeNotFoundException {
		Session session = getSessionFactory().openSession();
		Employee empPos = (Employee) session.get(Employee.class, name);
		if (empPos == null) {
			throw new EmployeeNotFoundException("EMployee not found ");

		}
		session.save(empPos);

		// TODO Auto-generated method stub
		return empPos;
	}

	@Override
	public boolean saveEmployeeDetails(int id, String name, String designation, Boolean isProbation,
			String totalLeavesAvailable, String noOfLeavesAvailed, Boolean isApprovedLeave)
			throws EmployeeNotSavedException {
		Session session = getSessionFactory().openSession();
		Employee e = new Employee();
		e.setDesignation(designation);
		e.setIsApprovedLeave(isApprovedLeave);
		e.setIsProbation(isProbation);
		e.setName(name);
		e.setNoOfLeavesAvailed(noOfLeavesAvailed);
		e.setTotalLeavesAvailable(totalLeavesAvailable);
		session.save(e);

		return true;

	}

	@Override
	public Employee updateEmployeeDetailsById(int id, String name, String designation, Boolean isProbation,
			String totalLeavesAvailable, String noOfLeavesAvailed, Boolean isApprovedLeave)
			throws EmployeeNotFoundException {
		Session session = getSessionFactory().openSession();
		Employee emp = (Employee) session.get(Employee.class, id);
		if (emp != null) {
			emp.setDesignation(designation);
			emp.setIsApprovedLeave(isApprovedLeave);
			emp.setIsProbation(isProbation);
			emp.setName(name);
			emp.setNoOfLeavesAvailed(noOfLeavesAvailed);
			emp.setTotalLeavesAvailable(totalLeavesAvailable);

		} else if (emp == null) {
			throw new EmployeeNotFoundException("Employee not found");
		}
		session.update(emp);
		return emp;
	}

	/*
	 * private static SessionFactory buildSessionFactory() { // Creating
	 * Configuration Instance & Passing Hibernate Configuration File Configuration
	 * configObj = new Configuration().con;
	 * configObj.configure("hibernate.cfg.xml");
	 * 
	 * // Since Hibernate Version 4.x, ServiceRegistry Is Being Used //
	 * ServiceRegistry serviceRegistryObj = new
	 * StandardServiceRegistryBuilder().applySettings(configObj.getProperties()).
	 * build();
	 * 
	 * // Creating Hibernate SessionFactory Instance sessionFactoryObj =
	 * configObj.buildSessionFactory(serviceRegistryObj); return sessionFactoryObj;
	 * }
	 */

}
