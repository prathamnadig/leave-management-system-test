package com.lms.dao;

import java.util.List;

import com.lms.entity.Employee;
import com.lms.entity.EmployeeLeave;

public interface EmployeeDao {
	
	public void addEmployee(Employee e);
	
	public EmployeeLeave getRemainingNoOfLeavesById(int id);
	
	public String getRemainingNoOfLeavesByName(String name);

	public EmployeeLeave getTotalNoOfLeaves(int id);

	public List<EmployeeLeave> displayAllEmployeeRemainingLeaves();

	public Employee getTotalNoOfLeavesInProbationById(int id);
	
	public String getStatusOfLeavesAppliedById(int id);

	public EmployeeLeave applyForLeave(int Id, String type,String FromDate,String ToDate);
}
